export class ErrorHandler {
    private readonly message: string
    private readonly response: Response
    constructor(dataType: string, response: Response) {
        this.message = dataType
        this.response = response
    }

    public handle() {
        if (!this.response.ok) {
            throw new Error(`Error ${this.message} ${this.response.url}. Error status: ${this.response.status}`)
        }
    }
}
