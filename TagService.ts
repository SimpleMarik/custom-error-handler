import {Tag} from "../types/Tags/Tag";
import {ErrorHandler} from "./ErrorHandler";
import {UrlService} from "../connector/UrlService";

export class TagService extends UrlService {
    constructor(address: string) {
        super(address);
    }

    public async getTags(): Promise<Tag[]> {
        try {
            const tagResponse = await fetch(this.tagsUrl, {
                headers: {
                    'Content-Type': 'application/json',
                }
            });
            new ErrorHandler('fetching tags from', tagResponse).handle()
            return await tagResponse.json()
        } catch (fetchError) {
            throw fetchError;
        }
    }

    public async createTag(name: string, color: string): Promise<void> {
        try {
            const response = await fetch(this.tagsUrl, {
                method: 'POST',
                body: JSON.stringify({name, color}),
                headers: {
                    'Content-Type': 'application/json',
                }
            })
            new ErrorHandler('creating new tag by', response).handle()
        } catch (fetchError) {
            console.error(fetchError)
            throw fetchError
        }
    }

    public async deleteTag(id: string): Promise<void> {
        try {
            const response = await fetch(this.tagsUrl + '/' + id, {
                method: 'DELETE',
                headers: {
                    'Content-Type': 'application/json',
                }
            })
            new ErrorHandler('deleting tag by', response).handle()
        } catch (fetchError) {
            console.error(fetchError)
            throw fetchError
        }
    }

    public async updateTag(id: string, name: string, color: string): Promise<void> {
        try {
            const response = await fetch(this.tagsUrl, {
                method: 'PUT',
                body: JSON.stringify({name, color, id}),
                headers: {
                    'Content-Type': 'application/json',
                }
            })
            new ErrorHandler('updating tag by', response).handle()
        } catch (fetchError) {
            console.error(fetchError)
            throw fetchError
        }
    }
}
